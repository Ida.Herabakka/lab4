package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int columns;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        grid = new CellState[rows][columns];
        for (int row = 0; row < rows; row++) {
			for (int col = 0; col < columns; col++) {
                grid[row][col] = initialState;
            }
        }
        

		
	}

    @Override
    public int numRows() {
        
        return rows;
    }

    @Override
    public int numColumns() {
        
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if(row < 0 || column < 0){
            throw new IllegalArgumentException("there is no row or column with negative value");
        }
        else if(row > this.rows || column > this.columns){
            throw new IndexOutOfBoundsException("Illegal row or column value");

        }
        this.grid[row][column] = element;
        
        
    }

    @Override
    public CellState get(int row, int column) {
        
        if(row < 0 || column < 0){
            throw new IllegalArgumentException("there is no row or column with negative value");
        }
        else if(row > this.rows || column > this.columns){
            throw new IndexOutOfBoundsException("Illegal row or column value");

        }
        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        
        IGrid newGrid = new CellGrid(this.rows, this.columns, CellState.DEAD);
        for (int row = 0; row < rows; row++) {
			for (int col = 0; col < columns; col++) {
                newGrid.set(row, col, get(row, col));
                
            }
        }


        return newGrid;
    }
    
}
