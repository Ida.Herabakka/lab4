package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    /**
	 * The grid of cells
	 */
	IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }



	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		
		IGrid nextGeneration = currentGeneration.copy();
		int rows = numberOfRows();
		int columns = numberOfColumns();
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < columns; col++){
				CellState newCellState = getNextCell(row, col);
				nextGeneration.set(row, col, newCellState);

				}

			}
		currentGeneration = nextGeneration;
		// TODO
	}

	@Override
	public CellState getNextCell(int row, int col) {
		/*Rules
		En levende celle blir døende
        En døende celle blir død
        En død celle med akkurat 2 levende naboer blir levende
        En død celle forblir død ellers */
		if(getCellState(row, col).equals(CellState.ALIVE)){
			return CellState.DYING;
		}
		else if(getCellState(row, col).equals(CellState.DEAD) && countNeighbors(row, col, CellState.ALIVE)==2){
			return CellState.ALIVE;
		}
		// TODO
		return CellState.DEAD;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int neighbors = 0;
		for(int r = row-1; r<row+2; r++){
			for(int c = col-1; c < col+2; c++){
				if(r == row && c == col || r< 0 || c<0 || r>numberOfRows() || c>numberOfColumns()){
					continue;
				}
				else if(getCellState(r, c).equals(state)){
					neighbors ++;
				}
			}
		}

		// TODO
		return neighbors;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}


}
